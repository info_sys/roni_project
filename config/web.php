<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'Imd1KqsGyw9YBW9ZTyBlc2Wlz7iuiWfx',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\user',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,//set this property to false to send mails to real email addresses
'transport' => [
    'class' => 'Swift_SmtpTransport',
    'host' => 'smtp.gmail.com',
    'username' => 'testfly75@gmail.com',
    'password' => 'Zaq1zaq1',
    'port' => '587',
    'encryption' => 'tls',
    ],
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],      
      
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'users' => 'user/index',
                'user/<id:\d+>' => 'user/view',
                'user/update/<id:\d+>' => 'user/update',
                'user/delete/<id:\d+>' => 'user/delete', 
                 'pupils' => 'pupil/index',
                'pupil/<id:\d+>' => 'pupil/view',
                'pupil/update/<id:\d+>' => 'pupil/update',
                'pupil/delete/<id:\d+>' => 'pupil/delete',     
                  'teams' => 'team/index',
                'team/<id:\d+>' => 'pupil/view',
                'team/update/<id:\d+>' => 'team/update',
                'team/delete/<id:\d+>' => 'team/delete', 
                      'activities' => 'activity/index',
                'activity/<id:\d+>' => 'activity/view',
                'activity/update/<id:\d+>' => 'activity/update',
                'activity/delete/<id:\d+>' => 'activity/delete',                     

            ],
        ],
        ],
        

  'modules' => [
            'gridview' =>  [
        'class' => '\kartik\grid\Module'
        // enter optional module parameters below - only if you need to  
        // use your own export download action or custom translation 
        // message source
        // 'downloadAction' => 'gridview/export/download',
        // 'i18n' => []
          ]

    ],



    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
//    $config['bootstrap'][] = 'debug';
//    $config['modules']['debug'] = [
//        'class' => 'yii\debug\Module',
//    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
