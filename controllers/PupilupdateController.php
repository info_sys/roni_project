<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;


class PupilupdateController extends Controller
{
	public function actionAddrule()
	{
		$auth = Yii::$app->authManager;				
		$updateOwnUser = $auth->getPermission('updateOwnPupil');
		//$auth->remove($updateOwnUser);
		
		$rule = new \app\rbac\OwnPupilRule;
		$auth->add($rule);
				
		$updateOwnPupil->rule_name = $rule->name;		
		$auth->add($updateOwnPupil);	
	}
}