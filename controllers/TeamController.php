<?php

namespace app\controllers;

use Yii;
use app\models\Team;
use app\models\TeamSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\growl\Growl;
use yii\web\UnauthorizedHttpException;
use app\models\User;



/**
 * TeamController implements the CRUD actions for Team model.
 */
class TeamController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Team models.
     * @return mixed
     */
    public function actionIndex()
    {
              if (!\Yii::$app->user->can('indexTeams'))
            throw new UnauthorizedHttpException ('Hey, You are not allowed to see teams');
        $searchModel = new TeamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider, 
                'ids' => Team::getcoord(),
    			'id' => $searchModel->id,
                'guides' => Team::getguide(),
    			'guide' => $searchModel->id,
          


        ]);
    }


    /**
     * Displays a single Team model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {

//         echo Growl::widget([
//     'type' => Growl::TYPE_SUCCESS,
//     'icon' => 'glyphicon glyphicon-ok-sign',
//     'title' => 'Note',
//     'showSeparator' => true,
//     'body' => 'great! you have  just ceated a new team'
// ]);

 if (!\Yii::$app->user->can('indexTeams'))
            throw new UnauthorizedHttpException ('Hey, You are not allowed to see teams');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Team model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!\Yii::$app->user->can('deleteUser'))
            throw new UnauthorizedHttpException ('Hey, You are not allowed to create teams');
        $model = new Team();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->teamNumber]);
        } else {

            return $this->render('create', [
                'model' => $model,
         
             


            

            ]);
        }
    }

    /**
     * Updates an existing Team model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!\Yii::$app->user->can('deleteUser'))
            throw new UnauthorizedHttpException ('Hey, You are not allowed to update teams');
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->teamNumber]);
        } else {
                 return $this->render('update', [
                    'model' => $model,
              
                 

            ]);
        }
    }

    /**
     * Deletes an existing Team model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!\Yii::$app->user->can('deleteUser'))
            throw new UnauthorizedHttpException ('Hey, You are not allowed to delete teams');
        
        $this->findModel($id)->delete();
          
        return $this->redirect(['index']);
    }

    /**
     * Finds the Team model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Team the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Team::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
