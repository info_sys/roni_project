<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;


class RbacController extends Controller
{
	public function actionRole()
	{
		$auth = Yii::$app->authManager;				
		
		$guide = $auth->createRole('guide');
		$auth->add($guide);
		
		$coordinator = $auth->createRole('coordinator');
		$auth->add($coordinator);

		$admin = $auth->createRole('admin');
		$auth->add($admin);
	}

	public function actionGpermissions()
	{
		$auth = Yii::$app->authManager;

		$indexActivity = $auth->createPermission('indexActivity');
		$indexActivity->description = 'All users can view activities';
		$auth->add($indexActivity);

		$indexPupil = $auth->createPermission('indexPupil');
		$indexPupil->description = 'All users can view pupils';
		$auth->add($indexPupil);



		$viewOwnPupils = $auth->createPermission('viewOwnPupils');
		$viewOwnPupils->description = 'All guide can view own pupils';
		$auth->add($viewOwnPupils);
		
		$viewOwnActivity = $auth->createPermission('viewOwnActivity');
		$viewOwnActivity->description = 'View own activity';
		$auth->add($viewOwnActivity);

		$updateOwnPassword  = $auth->createPermission('updateOwnPassword');
		$updateOwnPassword->description = 'Every user can update his/her
									own password';
		$auth->add($updateOwnPassword);		


		$updateOwnUser  = $auth->createPermission('updateOwnUser');
		$updateOwnUser->description = 'Every user can update his/her
									own user';
		$auth->add($updateOwnUser);	

		
	}

 
	public function actionCpermissions()
	{
		$auth = Yii::$app->authManager;

		$indexTeams = $auth->createPermission('indexTeams');
		$indexTeams->description = 'coordinators and above can view teams';
		$auth->add($indexTeams);
		
		$createPupil = $auth->createPermission('createPupil');
		$createPupil->description = 'coordinator can create ';
		$auth->add($createPupil);
		
		$updateOwnPupil = $auth->createPermission('updateOwnPupil');
		$updateOwnPupil->description = 'coordintor can update own pupil ';
		$auth->add($updateOwnPupil);		
	}
	

	public function actionAdminpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$indexUser = $auth->createPermission('indexUser');
		$indexUser->description = 'admin can view all users';
		$auth->add($indexUser);

		$createUser = $auth->createPermission('createUser');
		$createUser->description = 'Admin can create new users';
		$auth->add($createUser);
		
		$updateUser = $auth->createPermission('updateUser');
		$updateUser->description = 'Admin can update all users';
		$auth->add($updateUser);

		$deleteUser = $auth->createPermission('deleteUser');
		$deleteUser->description = 'Admin can delete users';
		$auth->add($deleteUser);

		$deletePupil = $auth->createPermission('deletePupil');
		$deletePupil->description = 'Admin can delete pupils';
		$auth->add($deletePupil);

		$updatePassword = $auth->createPermission('updatePassword');
		$updatePassword->description = 'Admin can update password for 
									all users';

        $createTeams = $auth->createPermission('createTeams');
		$createTeams->description = 'Admin can create new teams';
		$auth->add($createTeams);


		$updateTeams = $auth->createPermission('updateTeams');
		$updateTeams->description = 'Admin can update teams';
		$auth->add($updateTeams);


        $deleteTeams = $auth->createPermission('deleteTeams');
		$deleteTeams->description = 'Admin can delete teams';
		$auth->add($deleteTeams);

		$createEvent = $auth->createPermission('createEvent');
		$createEvent->description = 'Admin can create new events';
		$auth->add($createEvent);


		$updateEvent = $auth->createPermission('updateEvent');
		$updateEvent->description = 'Admin can update events';
		$auth->add($updateEvent);


        $deleteEvent = $auth->createPermission('deleteEvent');
		$deleteEvent->description = 'Admin can delete event';
		$auth->add($deleteEvent);

		$updatePupil = $auth->createPermission('updatePupil');
		$updatePupil->description = 'Admin can update all pupils';
		$auth->add($updatePupil);
	}

	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
		
		$guide = $auth->getRole('guide');

		    ///$viewOwnActivity = $auth->getPermission('viewOwnActivity');
		//$auth->addChild($guide, $viewOwnActivity);

		$indexPupil = $auth->getPermission('indexPupil');
		$auth->addChild($guide, $indexPupil);

    

		$indexActivity = $auth->getPermission('indexActivity');
		$auth->addChild($guide, $indexActivity);

		$viewOwnPupils = $auth->getPermission('viewOwnPupils');
		$auth->addChild($guide, $viewOwnPupils);

		

        $updateOwnPassword = $auth->getPermission('updateOwnPassword');
		$auth->addChild($guide, $updateOwnPassword);

		$updateOwnUser = $auth->getPermission('updateOwnUser');
		$auth->addChild($guide, $updateOwnUser);		
		

		$coordinator = $auth->getRole('coordinator');
		$auth->addChild($coordinator, $guide);

	    $indexTeams = $auth->getPermission('indexTeams');
		$auth->addChild($coordinator, $indexTeams);

        $createPupil = $auth->getPermission('createPupil');
		$auth->addChild($coordinator, $createPupil);


		$updateOwnPupil = $auth->getPermission('updateOwnPupil');
		$auth->addChild($coordinator, $updateOwnPupil);		
		
		
		$admin = $auth->getRole('admin');
		$auth->addChild($admin, $coordinator);

		$indexUser = $auth->getPermission('indexUser');
		$auth->addChild($admin, $indexUser);		
	
		$createUser = $auth->getPermission('createUser');
		$auth->addChild($admin, $createUser);

		$updateUser = $auth->getPermission('updateUser');
		$auth->addChild($admin, $updateUser);
		
		$deletePupil = $auth->getPermission('deletePupil');
		$auth->addChild($admin, $deletePupil);		
		
		
		$deleteUser = $auth->getPermission('deleteUser');
		$auth->addChild($admin, $deleteUser);		

        $createTeams = $auth->getPermission('createTeams');
		$auth->addChild($admin, $createTeams);
	
		$updateTeams = $auth->getPermission('updateTeams');
		$auth->addChild($admin, $updateTeams);
		
		$deleteTeams = $auth->getPermission('deleteTeams');
		$auth->addChild($admin, $deleteTeams);	

        $createEvent = $auth->getPermission('createEvent');
		$auth->addChild($admin, $createEvent);

		$updateEvent = $auth->getPermission('updateEvent');
		$auth->addChild($admin, $updateEvent);
		
		$deleteEvent = $auth->getPermission('deleteEvent');
		$auth->addChild($admin, $deleteEvent);	


		$updatePupil = $auth->getPermission('updatePupil');
		$auth->addChild($admin, $updatePupil);

	
	}
}