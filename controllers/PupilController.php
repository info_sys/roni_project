<?php

namespace app\controllers;

use Yii;
use app\models\Pupil;
use app\models\Team;
use app\models\PupilSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;


/**
 * PupilController implements the CRUD actions for Pupil model.
 */
class PupilController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pupil models.
     * @return mixed
     */
    public function actionIndex()
    {
           if (!\Yii::$app->user->can('indexPupil'))
           throw new UnauthorizedHttpException ('Hey, You are not allowed to see Pupils ');
        $searchModel = new PupilSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
           'pupils' => pupil::getPupilsWithAllPupils(),
            'pupil' => $searchModel->pupil, 
              ]);
    }

    /**
     * Displays a single Pupil model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {         
      if (!\Yii::$app->user->can('viewOwnPupils'))

   
           throw new UnauthorizedHttpException ('Hey, You are not allowed to view Pupil ');   
        return $this->render('view', [
            'model' => $this->findModel($id),

        ]);
    }

    /**
     * Creates a new Pupil model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
           if (!\Yii::$app->user->can('createPupil'))
           throw new UnauthorizedHttpException ('Hey, You are not allowed to create Pupils');
        $model = new Pupil();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pupilId]);
        } else {
               $Pupils = pupil::getPupils(); 
        
            return $this->render('create', [
                'model' => $model,
                'Pupils' => $Pupils,
            ]);
        }
    }

    /**
     * Updates an existing Pupil model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
     if (!\Yii::$app->user->can('updatePupil'))
                throw new UnauthorizedHttpException ('Hey, You are not allowed to update pupils');
                $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pupilId]);
        } else {
             $Pupils = pupil::getPupils(); 
            return $this->render('update', [
                'model' => $model,
                'Pupils' => $Pupils,

            ]);
        }
    }

    /**
     * Deletes an existing Pupil model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
           if (!\Yii::$app->user->can('deletePupil'))
            throw new UnauthorizedHttpException ('Hey, You are not allowed to delete pupils');
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pupil model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pupil the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pupil::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
