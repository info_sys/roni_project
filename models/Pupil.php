<?php

namespace app\models;


use Yii;
use yii\helpers\ArrayHelper;
use app\models\Team;    


/**
 * This is the model class for table "pupil".
 *
 * @property integer $pupilId
 * @property string $firstname
 * @property string $lastname
 * @property integer $phoneNumber
 * @property string $fatherName
 * @property integer $fatherPhone
 * @property string $motherName
 * @property integer $motherPhone
 * @property integer $homePhone
 * @property string $initialQues
 * @property string $forms
 * @property integer $teamNumber
 */
class Pupil extends \yii\db\ActiveRecord
{
public $pupil;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pupil';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phoneNumber', 'fatherPhone', 'motherPhone', 'homePhone', 'teamNumber'], 'integer'],
            [[ 'firstname', 'lastname', 'initialQues', 'forms'], 'required'],

            [['firstname', 'lastname', 'fatherName', 'motherName', 'initialQues', 'forms'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pupilId' => 'Pupil ID',
            'firstname' => 'First Name',
            'lastname' => 'Last Name',
            'phoneNumber' => 'Phone Number',
            'fatherName' => 'Father Name',
            'fatherPhone' => 'Father Phone',
            'motherName' => 'Mother Name',
            'motherPhone' => 'Mother Phone',
            'homePhone' => 'Home Phone',
            'initialQues' => 'Initial Ques',
            'forms' => 'Forms',
            'teamNumber' => 'Team',
        ];
    }
     public static function getPupils()
    {
        $pupils = ArrayHelper::
                    map(self::find()->all(), 'pupilId', 'firstname');
        return $pupils;                      
    }
        public static function getPupilsWithAllPupils()
    {
        $allPupils = self::getPupils();
        $allPupils[-1] = 'All pupils';
        $allPupils = array_reverse ( $allPupils, true );
        return $allPupils;    
    } 
    public function getTeamNum()
    {
        return $this->hasOne(Team::className(), ['teamNumber' => 'teamNumber']);
    }  
    public function getPupilname()
    {
        return $this->firstname.' '.$this->lastname;
    }
        public function getActivity()
    {
        return $this->hasMany(activity::className(), ['activityNumber' => 'activityNumber']);
    }
}


