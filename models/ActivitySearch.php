<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Activity;

/**
 * ActivitySearch represents the model behind the search form about `app\models\Activity`.
 */
class ActivitySearch extends Activity
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['activityNumber', 'date', 'pupil'], 'integer'],
            [['subject', 'content', 'difficult', 'goodMoments', 'lessonsLearned', 'generalImprission', 'expectations', 'comments'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
  
 public function search($params)
    {
       $this->load($params);
		
        if (\Yii::$app->user->can('updateUser')){
			$query = Activity::find();
		}else{
        $query = Activity::find()
                    ->select('*')
                ->from('activity','user')
                ->where(['activity.created_by' =>\Yii::$app->user->id ])
                 ->orwhere(['activity.created_by' =>\Yii::$app->user->id ]);
		}


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'activityNumber' => $this->activityNumber,
            'date' => $this->date,
            'pupil' => $this->pupil,
        ]);

        $query->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'difficult', $this->difficult])
            ->andFilterWhere(['like', 'goodMoments', $this->goodMoments])
            ->andFilterWhere(['like', 'lessonsLearned', $this->lessonsLearned])
            ->andFilterWhere(['like', 'generalImprission', $this->generalImprission])
            ->andFilterWhere(['like', 'expectations', $this->expectations])
            ->andFilterWhere(['like', 'comments', $this->comments]);

        return $dataProvider;
    }
}
