<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pupil;
use app\models\User;

/**
 * PupilSearch represents the model behind the search form about `app\models\Pupil`.
 */
class PupilSearch extends Pupil
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pupilId', 'phoneNumber', 'fatherPhone', 'motherPhone', 'homePhone', 'teamNumber'], 'integer'],
            [['firstname', 'lastname', 'fatherName', 'motherName', 'initialQues', 'forms'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
   public function search($params)
    {
       $this->load($params);
		
        if (\Yii::$app->user->can('updateUser')){
			$query = Pupil::find();
		}else{
        $query = Pupil::find()
                    ->select('*')
                ->from('pupil','team','user')
                ->innerJoin('team','team.teamNumber = pupil.teamNumber')
                ->where(['team.guideId1' =>\Yii::$app->user->id ])
                 ->orwhere(['team.id' =>\Yii::$app->user->id ]);
		}
        // $query = Pupil::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pupilId' => $this->pupilId,
            'phoneNumber' => $this->phoneNumber,
            'fatherPhone' => $this->fatherPhone,
            'motherPhone' => $this->motherPhone,
            'homePhone' => $this->homePhone,
            'teamNumber' => $this->teamNumber,
        ]);

        $query->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'fatherName', $this->fatherName])
            ->andFilterWhere(['like', 'motherName', $this->motherName])
            ->andFilterWhere(['like', 'initialQues', $this->initialQues])
            ->andFilterWhere(['like', 'forms', $this->forms]);

        return $dataProvider;
    }
}
