<?php

namespace app\models;



use Yii;
use yii\helpers\ArrayHelper;  
use app\models\Pupil;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "activity".
 *
 * @property integer $activityNumber
 * @property integer $date
 * @property string $subject
 * @property string $content
 * @property string $difficult
 * @property string $goodMoments
 * @property string $lessonsLearned
 * @property string $generalImprission
 * @property string $expectations
 * @property string $comments
 * @property string $pupil
 */
class Activity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['date'], 'safe'],
            ['pupil', 'required'],

            // ['Pupil', 'required'],
            [['subject', 'content', 'difficult', 'goodMoments', 'lessonsLearned', 'generalImprission', 'expectations', 'comments'], 'string'],
           // [['pupil'], 'string', 'max' => 255],
//['name', 'exist', 'allowArray' => true, 'when' => function ($model, $attribute) {return is_array($model->$attribute);}],
           // ['pupil', 'each' , 'rule' => ['string'], 'allowArray' => true ],
            //  [['pupil'], 'exist', 'allowArray' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'activityNumber' => 'Activity Number',
            'date' => 'Date',
            'subject' => 'Subject',
            'content' => 'Content',
            'difficult' => 'Difficult',
            'goodMoments' => 'Good Moments',
            'lessonsLearned' => 'Lessons Learned',
            'generalImprission' => 'General Imprission',
            'expectations' => 'Expectations',
            'comments' => 'Comments',
            'pupil' => 'Pupils',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
     public function behaviors()
    {
        return 
        [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
             ],
                'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }


   public static function getActivityPupil()
    {
         $pupil =  ArrayHelper::map(pupil::find()->all(),'pupilId','pupilname');
        return $pupil;                       
    }


    public function getPupilsbyactivity()
    {
        return $this->hasMany(pupil::className(), ['pupilId' => 'pupil']);
    }  
     public function getPupilsby() 
    {
        $pupil = pupil::find()->one();
    }

    public function getPupilsnames(){
        $str = $this->pupil;

        $arr = explode(",", $str);
        $arrLength = count($arr);
        for($i=0;$i<=$arrLength-1; $i++){
            $id = intval($arr[$i]);
            $name = \app\models\Pupil::findOne($id)->pupilname;
            $arr[$i] = $name;
        }
        $names = implode("<br>",$arr);
       return $names;

    }



public function beforeSave($insert)
    {
        // $pupil = ArrayHelper::map(pupil::find()->all(),'pupil','firstname'); 

        if (parent::beforeSave($insert)) {
//var_dump($_POST[''][]);
// var_dump($_POST['Activity']['pupil']);
//var_dump($insert);
            $pupilArray = $_POST['Activity']['pupil'];
            $pupilString = implode(",",array_values($pupilArray));
            // echo $pupilString;
           $_POST['Activity']['pupil']= $pupilString;
           //$this->pupil = serialize($_POST['Activity']['pupil']);
           $this->pupil = $pupilString;

            return true;
        } else {
            return false;
        }
    }

}
