<?php

namespace app\models;


use Yii;
use \yii\web\IdentityInterface;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;    


/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $auth_key
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $institute
 * @property integer $birthday
 * @property integer $phoneNumber
 * @property integer $yearGrade
 * @property integer $sizeShirt
 * @property string $fieldStudy
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{

    public $role;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phoneNumber', 'yearGrade',  'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['username', 'password', 'auth_key','sizeShirt', 'firstname', 'lastname', 'email', 'institute', 'fieldStudy'], 'string', 'max' => 255],
            ['role', 'safe'],
             ['birthday', 'required'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'auth_key' => 'Auth Key',
            'firstname' => 'First Name',
            'lastname' => 'Last Name',
            'email' => 'Email',
            'institute' => 'Institute',
            'birthday' => 'Birthday',
            'phoneNumber' => 'Phone Number',
            'yearGrade' => 'Year Grade',
            'sizeShirt' => 'Size Shirt',
            'fieldStudy' => 'Field Study',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'role' => 'User Role',
        ];
    }
        public function behaviors()
    {
        return 
        [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
             ],
                'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }
    public function getUserole()
    {
        $roleArray = Yii::$app->authManager->
                    getRolesByUser($this->id);
        $role = array_keys($roleArray)[0];              
        return  $role;
    }
        public static function getUsers()
    {
        $users = ArrayHelper::
                    map(self::find()->all(), 'id', 'fullname');
        return $users;                      
    }
 
public function getFullname()
    {
        return $this->firstname.' '.$this->lastname;
    }


        public static function getRoles()
    {
        $rolesObjects = Yii::$app->authManager->getRoles();
        $roles = [];
        foreach($rolesObjects as $id =>$rolObj){
            $roles[$id] = $rolObj->name; 
        }
        return $roles;  
    }
    public static function getUsersWithAllUsers()
    {
        $users = self::getUsers();
        $users[-1] = 'All Users';
        $users = array_reverse ( $users, true );
        return $users;  
    }   
    
    public static function getRolesWithAllRoles()
	{
		$roles = self::getRoles();
		$roles[-1] = 'All Roles';
		$roles = array_reverse ( $roles, true );
		return $roles;	
	}	

     public static function getUsersWithRoleCoordiator()
    {
        $uc = Yii::app()->db->createCommand()
         ->select('user.username')
         ->from('user','auth_assignment','auth_item')
         ->innerJoin('auth_assignment','user.id=auth_assignment.user_id')
         ->innerJoin('auth_item','auth_item.name = auth_assignment.item_name')
         ->where(['auth_assignment.item_name' => 'coordinator']
        // ->where(['like','auth_assignment.item_name' , 'coordinator'] 
         )->all();
        $uc[-1] = 'Choose Coordinator';
        $uc = array_reverse ( $uc, true );
        return $uc;  
    }

   

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }
public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }
 
    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }
public function validatePassword($password)
    {
        return $this->isCorrectHash($password, $this->password); 
    }

    private function isCorrectHash($plaintext, $hash)
    {
        return Yii::$app->security->validatePassword($plaintext, $hash);
    }

    /**
     * @param string $authKey
     * @return boolean if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
  public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
                    generatePasswordHash($this->password);

        if ($this->isNewRecord)
            $this->auth_key = Yii::$app->security->generateRandomString(32);

        return $return;
    }


    public function afterSave($insert,$changedAttributes)
    {
        $return = parent::afterSave($insert, $changedAttributes);

        if (!\Yii::$app->user->can('updateUser')){
            return $return;
        }
        $auth = Yii::$app->authManager;
        $roleName = $this->role;
        $role = $auth->getRole($roleName);
        if (\Yii::$app->authManager->getRolesByUser($this->id) == null){
            $auth->assign($role, $this->id);
        } else {
            $db = \Yii::$app->db;
            $db->createCommand()->delete('auth_assignment',
                ['user_id' => $this->id])->execute();
            $auth->assign($role, $this->id);
        }

        return $return;
    }

}
