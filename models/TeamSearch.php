<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Team;

/**
 * TeamSearch represents the model behind the search form about `app\models\Team`.
 */
class TeamSearch extends Team
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teamNumber', 'hourActive', 'id','guideId1'], 'integer'],
            [['teamName', 'groupName', 'dayActive'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
   public function search($params)
    {
       $this->load($params);
		
        if (\Yii::$app->user->can('updateUser')){
			$query = Team::find();
		}else{
        $query = Team::find()
                    ->select('*')
                ->from('team','user')
                 ->where(['team.id' =>\Yii::$app->user->id ]);
		}


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->id == -1 ? $this->id = null : $this->id;
        $this->guideId1 == -1 ? $this->guideId1 = null : $this->guideId1; 

        // grid filtering conditions
        $query->andFilterWhere([
            'teamNumber' => $this->teamNumber,
            'hourActive' => $this->hourActive,
            'id' => $this->id,
            'guideId1' => $this->guideId1,
        ]);

        $query->andFilterWhere(['like', 'teamName', $this->teamName])
            ->andFilterWhere(['like', 'groupName', $this->groupName])
            ->andFilterWhere(['like', 'dayActive', $this->dayActive]);

        return $dataProvider;
    }
}
