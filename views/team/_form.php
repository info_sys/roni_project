<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Team;
use app\models\user;


/* @var $this yii\web\View */
/* @var $model app\models\Team */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="team-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'teamName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'groupName')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'dayActive')
->dropDownList(array('Sunday'=>'Sunday','Monday'=>'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday')) ?>
    
    <!-- <?= $form->field($model, 'hourActive')->textInput() ?> -->

 
 <?= $form->field($model, 'hourActive')->dropDownList(array('16:00'=>'16:00','16:30'=>'16:30','17:00'=>'17:00','17:30'=>'17:30','18:00'=>'18:00')) ?>
<?= $form->field($model, 'id')->
                dropDownList(team::getcoord()) ?>  

<?= $form->field($model, 'guideId1')->
                dropDownList(team::getguide()) ?>  
          
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
