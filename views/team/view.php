<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Team;
use app\models\User;
use app\models\Pupil;
// use kartik\widgets\Select2;



/* @var $this yii\web\View */
/* @var $model app\models\Team */

$this->title = $model->teamName;
$this->params['breadcrumbs'][] = ['label' => 'Teams', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->teamNumber], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->teamNumber], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'teamNumber',
            'teamName',
            'groupName',
             [ 
                'label' => $model->attributeLabels()['guideId1'],
                'format' => 'html',
                'value' => is_object($model->userGuide) ?  
                        Html::a($model->userGuide->fullname,['user/view', 'id' => $model->userGuide->id]) : 'No one!',      
            ], 
             [ 
                'label' => $model->attributeLabels()['id'],
                'format' => 'html',
                'value' => is_object($model->userCoord) ?  
                        Html::a($model->userCoord->fullname,['user/view', 'id' => $model->userCoord->id]) : 'No one!', 
            ], 
              'dayActive',
              'hourActive',
              [ 
                'label' => 'Pupils',
                'format' => 'html',
                 'value' => isset($model->pupilTeam) ?  
                implode("<br>",\yii\helpers\ArrayHelper::map($model->pupilTeam, 'pupilId', 'pupilname')) : 'No one!',
            ],

           
           

        ],
    ]) ?>

</div>
