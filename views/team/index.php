<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use app\models\pupil;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TeamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Teams';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-index">

   <h1><?= Html::encode($this->title) ?></h1>
   <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?php if (\Yii::$app->user->can('createUser')) { ?>     

   <p>   
       <?= Html::a('Create Team', ['create'], ['class' => 'btn btn-success']) ?>
   </p>


    
        <?php
                $gridColumns = [
                'teamNumber',
                'teamName',
                'groupName',
                'dayActive',
                'hourActive',
                'id',  
                'guideId1',         
            ];
                    // Renders a export dropdown menu
        echo ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns
        ]);
      }
        ?> 

   <?= GridView::widget([
       'dataProvider' => $dataProvider,
       'filterModel' => $searchModel,
       'columns' => [
           ['class' => 'yii\grid\SerialColumn'],

        //    'teamNumber',
           'teamName',
           'groupName',
           'dayActive',
           'hourActive',
 
            //'id',
             [
				'attribute' => 'id',
				'label' => 'Coordinator',
				'format' => 'raw',
				'value' => function($model){
      if (is_object($model->userCoord)) {

            return  Html::a($model->userCoord->fullname,
                    ['user/view', 'id' => $model->userCoord->id]);
          }
          else{
            return 'no one';
          }
        },  


		      'filter'=>Html::dropDownList('TeamSearch[id]', $id, $ids, ['class'=>'form-control']),
			],
          [
				'attribute' => 'guideId1',
				'label' => 'Guide',
				'format' => 'raw',
				'value' => function($model){
          if (is_object($model->userGuide)) {

            return Html::a($model->userGuide->fullname,
                ['user/view', 'id' => $model->userGuide->id]);
          }
          else{
            return 'no one';
          }
				},
				'filter'=>Html::dropDownList('TeamSearch[guideId1]', $guide, $guides, ['class'=>'form-control']),
],

            //'guideId1',

['class' => 'yii\grid\ActionColumn',
           'visibleButtons' => [
          'delete' => (\yii::$app->user->can('createUser')),
          'update' => (\yii::$app->user->can('createUser')),
          'view' => (\yii::$app->user->can('indexTeams')),

           ],            
           ],
             ],
    ]); ?>


</div>