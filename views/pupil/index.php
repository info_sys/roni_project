<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PupilSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pupils';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pupil-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<?php if (\Yii::$app->user->can('createPupil')) { ?>     
    <p>
        <?= Html::a('Create Pupil', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

 

     <?php
                $gridColumns = [
                'teamNumber',
              //  'teamName',
               // 'groupName',
              //  'dayActive',
//'hourActive',
               // 'coordinatorId', 
               'pupilId',
                'firstname',
                'lastname',
                'phoneNumber',
                'fatherName',
                'fatherPhone',
                'motherName',
                'motherPhone',
                'homePhone',
                'initialQues',
                'forms',
            ];
                    // Renders a export dropdown menu
        echo ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns
        ]);
        ?> 
         <?php } ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'pupilId',
            // 'teamNumber',
             [
				'attribute' => 'teamNumber',
				'label' => 'Teams',
				'format' => 'raw',
				'value' => function($model){
      if (is_object($model->teamNum)) {

            return  $model->teamNum->name;
          }
          else{
            return 'no one';
          }
        },  
			],
            'firstname',
            'lastname',
           // 'phoneNumber',
            // 'fatherName',
            // 'fatherPhone',
            // 'motherName',
            // 'motherPhone',
            // 'homePhone',
            // 'initialQues',
            // 'forms',
           

['class' => 'yii\grid\ActionColumn',
       
           'visibleButtons' => [
          'delete' => (\yii::$app->user->can('deletePupil')),
          'update' => (\yii::$app->user->can('updatePupil')),




      

           ],            
           ],
             ],
    ]); ?>


</div>