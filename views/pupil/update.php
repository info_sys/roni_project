<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pupil */

$this->title = 'Update Pupil: ' . $model->pupilId;
$this->params['breadcrumbs'][] = ['label' => 'Pupils', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pupilname, 'url' => ['view', 'id' => $model->pupilId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pupil-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
