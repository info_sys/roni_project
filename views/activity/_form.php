<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;    
use app\models\pupil;
use app\models\Activity;
use yii\bootstrap\Alert;

use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model app\models\Activity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-form">

    <?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'date')->widget(\yii\jui\DatePicker::classname(), [
    'language' => 'en',
    'dateFormat' => 'yyyy-MM-dd',
]) ?>

    <?= $form->field($model, 'subject')->textarea(['rows' => 3]) ?>

   <?php $data = Activity::getActivityPupil(); ?>

   <?php
echo Alert::widget([
    'options' => [
        'class' => 'alert-info',
    ],
    'body' => 'please add  all the pupils that were on this activity',
]);
?>

  <?=  $form->field($model, 'pupil')->widget(Select2::classname(), [
   
    'data' => [$data],
    'language' => 'de',
   'options' => ['multiple' => true],
    'pluginOptions' => [
        'allowClear' => true,
        'tags' => true,
    ],
]);
?>

    <?= $form->field($model, 'content')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'difficult')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'goodMoments')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'lessonsLearned')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'generalImprission')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'expectations')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'comments')->textarea(['rows' => 3]) ?>

 
 

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
