<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\User;
use kartik\growl\Growl;
use kartik\export\ExportMenu;


/* @var $this yii\web\View */
/* @var $searchModel app\models\userSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>




    <?php if (\Yii::$app->user->can('createUser')) { ?>     

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
                $gridColumns = [
                'id',
                'username',
                'firstname',
                'lastname',
               'email:email',
                'institute',
                'birthday',
                'phoneNumber',
                'yearGrade',
                'sizeShirt',
                'fieldStudy',
                'created_at',
                'updated_at',
                'created_by',
                'updated_by',          
            ];
                    // Renders a export dropdown menu
        echo ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns
        ]);
    }
        ?> 


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
        
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
           // 'password',
            //'auth_key',
            
            'firstname',
             'lastname',
            //'role',
            //   [
			// 	'attribute' => 'role',
			// 	'label' => 'Role',
			// 	'format' => 'raw',
			// 	'value' => function($model){
			// return array_keys(\Yii::$app->authManager->getRolesByUser($model->id))[0];					
			// 	},
			// 	'filter'=>Html::dropDownList('UserSearch[role]', $role, $roles, ['class'=>'form-control']),
			// ],
            'email:email',
            // 'institute',
            // 'birthday',
             'phoneNumber',

            // 'yearGrade',
            // 'sizeShirt',
            // 'fieldStudy',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

['class' => 'yii\grid\ActionColumn',
           'visibleButtons' => [
          'delete' => (\yii::$app->user->can('createUser')),
          'update' => (\yii::$app->user->can('createUser')),
          'view' => (\yii::$app->user->can('createUser')),

           ],            
           ],
             ],
    ]); ?>


</div>
