<?php

use yii\db\Migration;

class m160927_094532_alter_user_table extends Migration
{

 public function up()
    {
        $this->addColumn('user','firstname','string');
        $this->addColumn('user','lastname','string');
        $this->addColumn('user','email','string');
        $this->addColumn('user','institute','string');
        $this->addColumn('user','birthday','date');
        $this->addColumn('user','phoneNumber','integer');
        $this->addColumn('user','yearGrade','integer');
        $this->addColumn('user','sizeShirt','string');
        $this->addColumn('user','fieldStudy','string');
        $this->addColumn('user','created_at','integer');
        $this->addColumn('user','updated_at','integer');
        $this->addColumn('user','created_by','integer');
        $this->addColumn('user','updated_by','integer');

    }

    public function down()
    {
        $this->dropColumn('user','firstname');
        $this->dropColumn('user','lastname');
        $this->dropColumn('user','email');
        $this->dropColumn('user','created_at');
        $this->dropColumn('user','updated_at');
        $this->dropColumn('user','created_by');
        $this->dropColumn('user','updated_by');
       $this->dropColumn('user','phoneNumber');
        $this->dropColumn('user','sizeShirt');
        $this->dropColumn('user','fieldStudy');
        $this->dropColumn('user','yearGrade');
        $this->dropColumn('user','birthday');
        $this->dropColumn('user','institute');







    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
