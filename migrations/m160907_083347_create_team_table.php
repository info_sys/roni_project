<?php

use yii\db\Migration;

/**
 * Handles the creation for table `team`.
 */
class m160907_083347_create_team_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('team', [
            'teamNumber' => 'pk',
            'teamName' => 'string',
            'groupName' => 'string',
            'dayActive' => 'string',
            'hourActive' => 'string',
            'id' => 'integer',  //from user table
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('team');
    }
}
