<?php

use yii\db\Migration;

class m161005_075035_init_note_table extends Migration
{
 public function up()
    {
        $this->createTable(
        'note',
            [
                'id' => 'pk',
                'title' => 'string',
                'details' => 'string',
                'addtoevents' => 'int',
                'tag'=> 'string'

            ]
        );
    }

    public function down()
    {
        $this->dropTable('note');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}