<?php

use yii\db\Migration;

class m161008_084219_alter_team_table extends Migration
{
    public function up()
    {
        $this->addColumn('team','guideId1','integer');
        $this->addColumn('team','guideId2','integer');

    }

    public function down()
    {
        $this->dropColumn('team','guideId1');
        $this->dropColumn('team','guideId2');

       
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
