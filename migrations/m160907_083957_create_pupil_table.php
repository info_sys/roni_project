<?php

use yii\db\Migration;

/**
 * Handles the creation for table `pupil`.
 */
class m160907_083957_create_pupil_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('pupil', [
            'pupilId' => 'pk',
            'firstname' => 'string',
            'lastname' => 'string',
            'phoneNumber' => 'integer',
            'fatherName' => 'string',
            'fatherPhone' => 'integer',
            'motherName' => 'string',
            'motherPhone' => 'integer',
            'homePhone' => 'integer',
            'initialQues' => 'string',
            'forms' => 'string',
            'teamNumber' => 'integer',  //from team table
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('pupil');
    }
}
